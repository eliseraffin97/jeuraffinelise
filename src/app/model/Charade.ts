export class Charade {
    phrase: string;
    reponse: string;

    constructor(phrase: string, reponse: string) {
        this.phrase = phrase;
        this.reponse = reponse;
    }
}