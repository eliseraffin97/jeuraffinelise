import { Component } from '@angular/core';
import { Charade } from './model/Charade';
import { GenerateCharadeService } from './generateCharade.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  constructor(private generateCharadeService:GenerateCharadeService){}


  title = 'JeuRaffinElise';
  show = false;
  
  selectedCharade: Charade;

  generateCharade() {
    this.selectedCharade = this.generateCharadeService.generateRandomCharade();
    this.show = true;
  }

}
