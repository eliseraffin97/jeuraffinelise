import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class CompareResultService {

    compareResult(solution, saisie) {
        return solution.toLowerCase() === saisie.toLowerCase(); 
    }

    

}