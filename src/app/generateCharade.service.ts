import { Injectable } from '@angular/core';
import { Charade } from './model/Charade';

@Injectable({
    providedIn: 'root'
})

export class GenerateCharadeService {
    char1 = new Charade('Mon premier est le contraire de tard. Mon second est le contraire de laid. Mon troisième s’enfile aux mains quand on a froid.', 'Toboggan');
    char2 = new Charade('Mon premier est une partie d’un oiseau. Mon deuxième est une marque de bière bien connue. Mon troisième est composé de 52 semaines. Mon tout est un animal.', 'Elephant');
    char3 = new Charade('Mon premier porte les voiles d’un bateau. Mon deuxième est compris entre 1 et 5. Mon troisième se boit au petit déjeuner. Mon quatrième est la lettre qu’il faut enlever à Blanc pour que ça fasse Black. Mon tout est un petit gâteau.', 'Madeleine');
    char4 = new Charade('Mon premier est une marque de chaussures moches. Mon deuxième est indispensable à la vie. Mon troisième est une activité de vente illégale. Mon tout vit dans les fleuves d’Amazonie.', 'Crocodile');
    char5 = new Charade('Mon premier est en haut du t-shirt.Mon deuxième est le métal préféré des pirates. Mon troisième est la 9ème lettre de l’alphabet. Mon quatrième est le nombre d’années que l’on a. Mon tout est une activité artistique que les enfants adorent.', 'Coloriage');
    char6 = new Charade('Mon premier est révolu .Mon deuxième accueille les nageurs prêts à plonger. Mon troisième est un organe. Mon quatrième ne cesse de tourner. Mon tout a soif de connaissances.', 'Explorateur');
    char7 = new Charade('Mon premier est une lettre de l’alphabet. Mon deuxième recouvre tout notre corps.Mon troisième est un récipient. Mon quatrième est un pronom possessif. Mon cinquième est un pronom possessif. Mon tout est un animal pas commode.', 'Hippopotame');
    char8 = new Charade("Mon premier est le deuxième mot qu'emploi les marins pour hisser la grand-voile. Mon deuxième est une terre anglaise. Mon troisième est la 5ème lettre de l'alphabet. Mon tout est un pays d'Europe", 'Islande');
    char9 = new Charade("Mon premier est un féculent. Mon second est la deuxième lettre de l'alphabet. Mon troisième est le même que mon premier. Mon tout est un footballeur français.", 'Ribéry');
    char10 = new Charade("Mon premier est particulier ou d’école. Mon second s’exauce. Mon troisième relie Vierzon à Montauban. Mon tout est une réserve très utile", 'Cave à vin');
    listeCharades = [this.char1, this.char2, this.char3, this.char4, this.char5, this.char6, this.char7, this.char8, this.char9, this.char9];



    generateRandomCharade() {
        return this.listeCharades[Math.floor(Math.random() * (this.listeCharades.length))];
    }

}