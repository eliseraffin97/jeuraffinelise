import { Component, OnInit, Input, Output } from '@angular/core';
import { Charade } from '../model/Charade';
import { CompareResultService } from '../CompareResult.service';
import { EventEmitter } from 'protractor';
@Component({
  selector: 'app-charade',
  templateUrl: './charade.component.html',
  styleUrls: ['./charade.component.css']
})
export class CharadeComponent implements OnInit {

  @Input() charade: Charade;
  reponseSoumise: String;
  saisieUtilisateur = '';
  retour = '';
  score = 0;

  constructor(private compareResult:CompareResultService) { }

  ngOnInit(): void {
  }

  checkReponse() {
    if (this.saisieUtilisateur === '') {
      alert('Veuillez saisir une réponse');
    } else {
      if (this.compareResult.compareResult(this.charade.reponse, this.saisieUtilisateur)) {
        this.saisieUtilisateur = '';
        this.retour = 'Bien joué!';
        this.score += 1;
      } else {
        this.saisieUtilisateur = '';
        this.retour = "Dommage, ce n'est pas la bonne réponse";
      }
      alert(this.retour);
    }
  }

  showResult() {
    alert('La réponse est: ' + this.charade.reponse);
  }

}
